# Instructions To Get Server Online

* Download or Clone Git Repository
* Run `npm install`
* Run `npm start`

You'll have the server running on port 3336. ([http://localhost:3336/](http://localhost:3336/))

# Source Code

Source code is located under the `src` folder.

# Elements Used

Server is builted using ExpressJS and TypeScript.