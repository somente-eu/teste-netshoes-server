const fs = require('fs')

export default function (req: any, res: any) {
    let json = JSON.parse(fs.readFileSync('./dist/data.json', 'utf8'))
    res.json(json)
}