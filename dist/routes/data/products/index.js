"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const fs = require('fs');
function default_1(req, res) {
    let json = JSON.parse(fs.readFileSync('./dist/data.json', 'utf8'));
    res.json(json);
}
exports.default = default_1;
