'use strict';
Object.defineProperty(exports, "__esModule", { value: true });
const _1 = require("./routes/data/products/");
const express = require('express');
const app = express();
const cors = require('cors');
const bodyParser = require('body-parser');
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cors());
app.get('/products/list', _1.default);
app.get('/', (req, res) => {
    res.json({
        netshoes: 'Api developed by Adrian Prado.'
    });
});
app.listen(3336);
console.log('Server started at: http://localhost:3336');
